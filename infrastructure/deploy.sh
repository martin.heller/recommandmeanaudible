aws cloudformation create-stack --region eu-central-1 --stack-name recommandmeanaudible --capabilities CAPABILITY_NAMED_IAM --template-body file://./stack.yml
if [ $? = 0 ]
then
    aws cloudformation wait --region eu-central-1 stack-create-complete --stack-name recommandmeanaudible
else
    aws cloudformation update-stack --region eu-central-1 --stack-name recommandmeanaudible --capabilities CAPABILITY_NAMED_IAM --template-body file://./stack.yml
    if [ $? = 0 ]
    then
        aws cloudformation wait --region eu-central-1 stack-update-complete --stack-name recommandmeanaudible
    else
        exit $?
    fi
fi
