module.exports = function () {
    function map(col, call) {
        let back = [];
        for (let i=0; i<col.length; i++) {
            back[i] = call(col.item(i), i, col);
        }
        return back;
    }
    function context(val, call) {
        return call(val);
    }

    return map(
        document.querySelectorAll('li.bc-list-item.productListItem'),
        audible => ({
            title: (audible.querySelector('h3.bc-heading a') || {}).innerText,
            url: (audible.querySelector('h3.bc-heading a') || {}).href,
            image: context(audible.querySelector('img[data-bc-hires]'), node => node && node.attributes.getNamedItem('data-bc-hires').textContent),
            sample: context(audible.querySelector('button[data-mp3]'), node => node && node.attributes.getNamedItem('data-mp3').textContent),
            authors: map(audible.querySelectorAll('.authorLabel span a') || [], v => v.innerText),
            narrators: map(audible.querySelectorAll('.narratorLabel span a') || [], v => v.innerText),
            series: context(audible.querySelector('.seriesLabel span a'), series => series && {
                name: series.innerText,
                url: series.href,
                episode: Number(series.nextSibling.nodeValue.split(/\s+/)[2])
            })
        })
    );
};