const sleep = require('sleep-promise');
const puppeteer = require('puppeteer');
const browserScript = require('./browser_script');
const {writeFileSync} = require('fs');

async function openCategory({category, browser}) {
    const page = await browser.newPage();
    console.log('Going to audible.de...');
    await page.goto('https://www.audible.de/');
    console.log(`Selecting category "${category}"...`);
    await (await page.$x('//span[contains(text(), \'Hörbücher\')]'))[0].click();
    await (await page.$x(`//a[contains(text(), '${category}')]`))[0].click();
    await waitForNavigtion(page);
    console.log(`Selecting all audibles in category...`);
    await (await page.$x(`//a[contains(text(), 'Alle ${category} Hörbücher anzeigen')]`))[0].click();
    // await (await page.$x(`//a[contains(text(), 'Alle')]`))[0].click();
    // await page.click('a.allInCategoryPageLink');
    await waitForNavigtion(page);
    console.log(`Selecting pagesize 50...`);
    await page.select('select[name="pageSize"]', '50');
    await sleep(1000);
    return Promise.resolve(page);
}

async function gotoNextPage({page}) {
    console.log('Going to next page...');
    let nextButton = await page.$x('//span[contains(text(), "Eine Seite vorwärts")]');
    if (nextButton.length > 0) {
        await nextButton[0].click();
        await sleep(1000);
        return Promise.resolve(true);
    } else {
        console.log('No next page available.');
        return Promise.resolve(false);
    }
}

async function gotoPage({page, number}) {
    console.log(`Going to page ${number}...`);
    let nextButton = await page.$x(`//a[text()="${number}"]`);
    if (nextButton.length > 0) {
        await nextButton[0].click();
        await sleep(3000 + Math.random() * 3000);
        return Promise.resolve(true);
    } else {
        console.log('Page available.');
        return Promise.resolve(false);
    }
}

const parseAudibles = ({page}) => page.evaluate(browserScript);

async function crawlCategory({browser, category}) {
    const page = await openCategory({browser, category});

    let allAudibles = [];

    const PAGE_SIZE = 50;
    for (let i = 0; i < PAGE_SIZE; i++) {
        console.log(`Parsing page ${i}...`);
        let audibles = await parseAudibles({page});
        audibles = audibles.filter(
            audible =>
                audible.title &&
                audible.url &&
                audible.image &&
                audible.sample &&
                audible.authors.length > 0 &&
                audible.narrators.length > 0 &&
                audible.series
        );
        allAudibles = allAudibles.concat(audibles);
        // let nextPageAvailable = await gotoNextPage({page});
        let nextPageAvailable = await gotoPage({page, number: (i + 2)});
        if (!nextPageAvailable) {
            break;
        }
    }

    writeFileSync(`./${category}.json`, JSON.stringify(allAudibles, null, 2));
}

const waitForNavigtion = page => page.waitForNavigation({waitUntil: 'domcontentloaded'});

const categories = ['Thriller', 'Fantasy', /*'Science-Fiction',*/ 'Krimi', 'Romane', /*'Historische Romane'*/, 'Klassiker', /*'Comedy & Humor'*/];

const sequential = promises => promises.reduce((p, v) => p.then(v), Promise.resolve());

puppeteer.launch({headless: true})
    .then(
        browser => sequential(
            categories.map(category => () => crawlCategory({browser, category}))
        )
            .then(() => browser.close())
    )
    .then(console.log)
    .catch(console.error);


// const {writeFileSync} = require('fs');
// const fetch = require('node-fetch');
//
// const numberOfPagesToFollow = 10;
//
// const parseResponse = resp => Promise.resolve(resp.text())
//     .then(text => {
//         return Promise.all([
//             Promise.resolve(text.match(/data-mp3="(.*?)"/g))
//                 .then(matches => matches.map(path => ({
//                     url: path.match(/data-mp3="(.*?)"/)[1]
//                 }))),
//             Promise.resolve(text.match(/data-lazyload="(.*?)"/g))
//                 .then(matches => matches.map(path => ({
//                     pic: path.match(/data-lazyload="(.*?)"/)[1]
//                 }))),
//             Promise.resolve(text.match(/productListItem" aria-label='(.*?)'/g))
//                 .then(matches => matches.map(path => ({
//                     title: path.match(/productListItem" aria-label='(.*?)'/)[1]
//                 }))),
//             Promise.resolve(text.match(/bc-color-link" tabindex="0" aria-hidden='true' href="(.*?)">/g))
//                 .then(matches => matches.map(path => ({
//                     link: path.match(/bc-color-link" tabindex="0" aria-hidden='true' href="(.*?)">/)[1]
//                 })))
//         ])
//     })
//     .then(arrs => arrs[0].map((v, i) => arrs.reduce((p, v) => Object.assign(p, v[i]), {})));
//
// let fetches = [];
// for (let i = 1; i <= numberOfPagesToFollow; i++) {
//     fetches.push(
//         new Promise(resolve => setTimeout(resolve, (i - 1) * 1500))
//             .then(() =>
//                 // fetch('https://www.audible.de/a/search?ref=a_a_search_c4_pageSize_3&pf_rd_p=cc935d10-7a4f-4b11-bbdf-80ec5c6000ab&pf_rd_r=HF0WCEAHK3VZ34XZX7GA&&node=656378031&submitted=1&pageSize=50' + (i !== 1 ? '&page=' + i : ''), {
//                 fetch('https://www.audible.de/a/search?ref=a_a_search_c4_pageSize_3&pf_rd_p=cc935d10-7a4f-4b11-bbdf-80ec5c6000ab&pf_rd_r=VA32WBGSAH4GM4PB26DZ&&submitted=1&pageSize=50' + (i !== 1 ? '&page=' + i : ''), {
//                     headers: {Cookie: 'session-id=140-4618700-6115705; session-id-time=2082787201l; ubid-main=131-0563678-9827323; ipRedirectOverride=true; currentActionCode="ANONAUDW0378WS010202_05/30/2018 17:07"; orginalActionCode="ANONAUDW0378WS010202_05/30/2018 17:07"; session-token=RMZZFs+A6ZX2UtluYOCUo39wDDw/we2oBkJGt2JrZipwsB0JzAj3qi3B6qqA5zYMHm6FQzQoTNWqz1RJ9/VaYn/gxJBoaryUtZ7rfKpJ94zSDnKTSv7esmXtbDiLkI8BVs2I5GxUf8mTNFpzsvNp293NZPsE+c1gUX/+HuUmEkJ07YMC+gBsWj4zMLBWjDiq; AMCVS_165958B4533AEE5B0A490D45%40AdobeOrg=1; check=true; AAMC_audible_0=REGION%7C6; aam_uuid=55826957573378743824554688092151470594; AMCV_165958B4533AEE5B0A490D45%40AdobeOrg=-330454231%7CMCIDTS%7C17682%7CMCMID%7C55802820368187717034556829685820222267%7CMCAAMLH-1528304834%7C6%7CMCAAMB-1528304834%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1527707234s%7CNONE%7CMCAID%7C2B98BC008530EE58-60000304A0041155%7CMCSYNCSOP%7C411-17689%7CvVersion%7C3.1.2; cvo_sid1=PS5YN9RAEAKS; mbox=PC#b94387c6e08046ab81a9d41d7f291cbc.26_20#1590944836|session#4821fab438dd4b59b4f2a5c7030002ab#1527703925; _uetsid=_uet76a7acd0; cvo_tid1=B1C56fEMwuU|1527700036|1527702066|0; s_sess=%20s_cc%3Dtrue%3B%20s_ppvl%3Dadbl-best-sellers%252C11%252C11%252C671%252C1366%252C671%252C1366%252C768%252C1%252CP%3B%20s_ppv%3Dadbl-best-sellers%252C11%252C11%252C671%252C1366%252C150%252C1366%252C768%252C1%252CP%3B; s_pers=%20gpv_pn%3Dadbl-best-sellers%7C1527704031299%3B%20s_getNewRepeat%3D1527702231304-New%7C1530294231304%3B%20s_lv%3D1527702231306%7C1622310231306%3B%20s_lv_s%3DFirst%2520Visit%7C1527704031306%3B; s_sq=audiblecomproduction%3D%2526c.%2526a.%2526activitymap.%2526page%253Dadbl-best-sellers%2526link%253DBest%252520Sellers%2526region%253DnavTop_pl2_popover%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Dadbl-best-sellers%2526pidt%253D1%2526oid%253Dhttps%25253A%25252F%25252Fwww.audible.com%25252Fadblbestsellers%25253Fref%25253Da_adblbests_t1_navTop_pl2cg1c0r0%252526pf_rd_p%25253De619441d-0c42-4%2526ot%253DA; csm-hit=tb:XWKP8BYWZ79FSYDGMZZ9+s-MYRJ835VT1A1V1VTZXA3|1527702231327&adb:adblk_yes'},
//                     redirect: 'manual'
//                 })
//             )
//             .then(parseResponse)
//             .then(v => {
//                 console.log(`${i} of ${numberOfPagesToFollow} downloaded and parsed.`);
//                 return v;
//             })
//     );
// }
//
// Promise.all(fetches)
//     .then(parses => parses.reduce((p, v) => p.concat(v), []))
//     .then(audibles => {
//         writeFileSync('../frontend/src/audibles.json', JSON.stringify(audibles, null, 2));
//         console.log(`${audibles.length} audiobooks parsed`);
//     });
