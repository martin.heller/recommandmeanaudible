import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import i18next from "i18next";

it("renders without crashing", () => {
  i18next.init({});
  const div = document.createElement("div");
  ReactDOM.render(<App i18n={i18next} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
