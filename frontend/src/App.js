import React, { Component, Fragment } from "react";
import "./App.css";
import ReactAudioPlayer from "react-audio-player";
import { translate } from "react-i18next";
import Features from "./features/Features";
import Feature from "./features/Feature";

const audibles = {
  Thriller: require("./Thriller"),
  Fantasy: require("./Fantasy"),
  Krimi: require("./Krimi"),
  Klassiker: require("./Klassiker"),
  Romane: require("./Romane")
};

const getRandomAudible = category =>
  audibles[category][Math.floor(Math.random() * audibles[category].length)];

let gatewayUrl;
if (window.location.hostname === "localhost") {
  gatewayUrl = `https://gzufqq0ij8.execute-api.eu-central-1.amazonaws.com/prod`;
} else {
  gatewayUrl = `https://y05uemw1t3.execute-api.eu-central-1.amazonaws.com/prod`;
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      audible: {},
      showInfo: false,
      history: [],
      category: "Fantasy"
    };
  }

  getNewAudible() {
    let newHistory = this.state.history;
    if (this.state.audible.sample) {
      newHistory = [this.state.audible].concat(newHistory).slice(0, 5);
      window.localStorage.setItem("history", JSON.stringify(newHistory));
    }
    this.setState({
      audible: getRandomAudible(this.state.category),
      showInfo: false,
      history: newHistory
    });
  }

  getNewAudibleOnError() {
    console.log(`Couldn't play ${this.state.audible.sample}`);
    setTimeout(
      () =>
        this.setState({
          audible: getRandomAudible(this.state.category),
          showInfo: false
        }),
      100
    );
  }

  showInfo() {
    this.setState({ showInfo: true });
  }

  onAudioEnded() {
    this.showInfo();
  }

  onAudioEndedWithNewAudible() {
    this.showInfo();
    setTimeout(this.getNewAudible.bind(this), 5000);
  }

  updateCategory(e) {
    this.setState({ category: e.target.value });
  }

  componentWillMount() {
    if (window.localStorage) {
      let history = window.localStorage.getItem("history");
      if (history) {
        history = JSON.parse(history);
        this.setState({ history });
      }
    }
  }

  clearHistory() {
    window.localStorage.removeItem("history");
    this.setState({ history: [] });
  }

  componentDidMount() {
    this.getNewAudible();
  }

  render() {
    const { t = v => v } = this.props;
    let audioUrl = (this.state.audible.sample || "").replace(
      "https://samples.audible.de",
      gatewayUrl
    );

    return (
      <Features features={["autoContinue"]}>
        <div className="App">
          <header className="App-header">
            {this.state.showInfo && (
              <Fragment>
                <a target="_blank" rel="noopener noreferrer" href={this.state.audible.url}>
                  <img
                    src={this.state.audible.image}
                    className="App-logo"
                    alt="logo"
                  />
                  <h1 className="App-title">{this.state.audible.title}</h1>
                </a>
                <h3 className="App-subtitle">
                  <p>
                    {t("writtenBy")}
                    {(this.state.audible.authors || []).join(", ")}
                  </p>
                  <p>
                    {t("narratedBy")}
                    {(this.state.audible.narrators || []).join(", ")}
                  </p>
                </h3>
              </Fragment>
            )}
            {this.state.showInfo || (
              <Fragment>
                <h2 className="App-subtitle">
                  <p>{t("tagLine1")}</p>
                  <p>{t("tagLine2")}</p>
                </h2>
                <h4 className="App-subtitle" style={{ paddingTop: 100 }}>
                  <p>
                    {t("newSampleLabel")}
                    <button
                      onClick={this.getNewAudible.bind(this)}
                      style={{
                        marginLeft: 16,
                        background: "#00bb00",
                        border: "none",
                        borderRadius: 4,
                        padding: 4,
                        paddingLeft: 8,
                        paddingRight: 8
                      }}
                    >
                      {t("newSampleBtn")}
                    </button>
                  </p>
                  <p>
                    {t("categoryLabel")}
                    <select
                      style={{
                        marginLeft: 16,
                        background: "#bbbbbb",
                        border: "none",
                        borderRadius: 4,
                        padding: 4,
                        paddingLeft: 8,
                        paddingRight: 8
                      }}
                      onChange={this.updateCategory.bind(this)}
                      defaultValue={this.state.category}
                    >
                      <option>Thriller</option>
                      <option>Fantasy</option>
                      <option>Krimi</option>
                      <option>Romane</option>
                      <option>Klassiker</option>
                    </select>
                  </p>
                </h4>
              </Fragment>
            )}
          </header>
          <Feature expects="autoContinue">
            <ReactAudioPlayer
              src={audioUrl}
              autoPlay
              controls
              onEnded={this.onAudioEndedWithNewAudible.bind(this)}
              onError={this.getNewAudibleOnError.bind(this)}
            />
          </Feature>
          <Feature denies="autoContinue">
            <ReactAudioPlayer
              src={audioUrl}
              autoPlay
              controls
              onEnded={this.onAudioEnded.bind(this)}
              onError={this.getNewAudibleOnError.bind(this)}
            />
          </Feature>
          {this.state.history.length > 0 && (
            <div style={{ textAlign: "left", paddingLeft: 20 }}>
              <h3>
                {t("historyHeadline")}
                <button onClick={this.clearHistory.bind(this)}>
                  {t("clearHistoryBtn")}
                </button>
              </h3>
              {this.state.history.map(audible => (
                <a
                  key={audible.url}
                  target="_blank"
                  rel="noopener noreferrer"
                  style={{
                    float: "left",
                    width: "20%",
                    minWidth: 200,
                    textAlign: "center"
                  }}
                  href={audible.url}
                >
                  <img
                    src={audible.image}
                    className="App-logo"
                    alt="logo"
                    style={{ height: 150 }}
                  />
                  <p>{audible.title}</p>
                </a>
              ))}
            </div>
          )}
        </div>
      </Features>
    );
  }
}

export default translate("app")(App);
