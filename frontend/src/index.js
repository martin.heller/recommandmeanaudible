import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import en from "./translations/en";
import de from "./translations/de";

i18next.use(LanguageDetector).init({
  fallbackLng: "en",
  defaultNS: "app",
  resources: {
    en,
    de
  }
});

ReactDOM.render(<App i18n={i18next} />, document.getElementById("root"));
