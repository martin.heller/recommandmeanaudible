import * as React from "react";
import { Consumer } from "./Features";

export default ({ children, expects, denies }) => (
  <Consumer>
    {({ hasFeature }) =>
      (expects && hasFeature(expects)) || (denies && !hasFeature(denies))
        ? children
        : null
    }
  </Consumer>
);
