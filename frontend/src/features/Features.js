import * as React from "react";

function createState({ features, variantId }) {
  return {
    variantId,
    features,
    hasFeature: feature => features.indexOf(feature) !== -1
  };
}

function selectFeatures(features) {
  return features.filter(f => Math.random() < 0.5);
}

export const { Provider, Consumer } = React.createContext(createState([]));

function transformFeatures(features) {
  return features.sort().map(feature => {
    feature = feature.toLowerCase();
    if (!/^[a-z0-9_]+$/.test(feature)) {
      throw new Error(`Feature '${feature}' contains not allowed characters.`);
    }
    return feature;
  });
}

export default class Features extends React.Component {
  constructor(props) {
    super(props);

    const features = transformFeatures(props.features);
    const selectedFeatures = selectFeatures(features);

    this.state = {
      variantId: selectedFeatures.join(";"),
      features: selectedFeatures
    };
  }

  render() {
    const { children } = this.props;
    return <Provider value={createState(this.state)}>{children}</Provider>;
  }
}
