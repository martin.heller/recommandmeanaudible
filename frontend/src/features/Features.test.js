import Features from "./Features";

it("Features can contain characters, numbers and underscore", () => {
  const run = () =>
    new Features({
      features: ["asidHJflsidj", "jfs323092034", "sdfijs3093_sidfl"]
    });

  expect(run).not.toThrow();
});
it("Features will produce proper variantId", () => {
  const features = new Features({
    features: ["jfs323092034", "sdfijs3093_sidfl", "asidHJflsidj"]
  });

  expect(features.state.variantId).toEqual(features.state.features.join(";"));
});
it("Features cannot contain special characters", () => {
  const run = () =>
    new Features({
      features: ["!@#$%^&*()"]
    });

  expect(run).toThrow();
});
